﻿using System.Linq;
using System.Threading.Tasks;
using IPSENH_Test_Backend.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IPSENH_Test_Backend.Controllers
{
    public class TicketController: Controller
    {
        private readonly DataContext _context;
        
        public TicketController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _context.helloVersions.OrderBy(hv => hv.Version).FirstOrDefaultAsync());
        }
    }
}