﻿using System.Linq;
using System.Threading.Tasks;
using IPSENH_Test_Backend.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IPSENH_Test_Backend.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class Filler15 : Controller
    {
        private readonly DataContext _context;

        public Filler15(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get() {
            return Ok(await _context.helloVersions.OrderBy(hv => hv.Version).FirstOrDefaultAsync());
        }

        [HttpPost]
        public async Task<IActionResult> Post(HelloVersion helloVersionFromRequest) {

            HelloVersion helloVersionFromDB = await _context.helloVersions.OrderBy(hv => hv.Version).FirstOrDefaultAsync();

            if (helloVersionFromDB == null) {
                helloVersionFromDB = new HelloVersion();
                _context.helloVersions.Add(helloVersionFromDB); 
            }

            helloVersionFromDB.Message = helloVersionFromRequest.Message;
            helloVersionFromDB.Version = helloVersionFromRequest.Version;

            if (await _context.SaveChangesAsync() > 0)
            {
                return Ok(helloVersionFromDB);
            }
            else {
                return BadRequest("Niks veranderd");
            }

        }
    }
}