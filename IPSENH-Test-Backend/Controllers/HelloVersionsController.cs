﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IPSENH_Test_Backend;
using IPSENH_Test_Backend.Data;

namespace IPSENH_Test_Backend.Controllers
{
    [ApiController]
    [Route("/api/hello-world")]
    public class HelloVersionsController : Controller
    {
        private readonly IHelloVersionRepository _repo;
        public HelloVersionsController(IHelloVersionRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public async Task<IActionResult> Get() {
            return Ok(await _repo.Get()); ;
        }

        [HttpPost]
        public async Task<IActionResult> Post(HelloVersion helloVersionFromRequest) {

            bool success = await _repo.Set(helloVersionFromRequest);

            if (success)
            {
                return Ok();
            }
            else {
                return BadRequest("Niks veranderd");
            }

        }
    }
}
