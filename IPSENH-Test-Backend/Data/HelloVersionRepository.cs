﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPSENH_Test_Backend.Data
{
    public class HelloVersionRepository : IHelloVersionRepository
    {
        private readonly DataContext _context;

        public HelloVersionRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<HelloVersion> Get()
        {
            return _context.helloVersions.OrderBy(hv => hv.Version).FirstOrDefault();
        }

        public async Task<bool> Set(HelloVersion helloVersion)
        {
            HelloVersion helloVersionFromDB = _context.helloVersions.OrderBy(hv => hv.Version).FirstOrDefault();

            if (helloVersionFromDB == null)
            {
                helloVersionFromDB = new HelloVersion();
                _context.helloVersions.Add(helloVersionFromDB);
            }

            helloVersionFromDB.Message = helloVersion.Message;
            helloVersionFromDB.Version = helloVersion.Version;

            return await _context.SaveChangesAsync() > 0;
            
        }
    }
}
