﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPSENH_Test_Backend.Data
{
    public interface IHelloVersionRepository
    {
        public Task<HelloVersion> Get();

        public Task<bool> Set(HelloVersion helloVersion);
    }
}
