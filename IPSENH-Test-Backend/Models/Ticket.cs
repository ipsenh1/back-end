﻿using System;

namespace IPSENH_Test_Backend
{
    public class Ticket
    {
        public Guid Id { get; set; }
        
        public String Owner { get; set; }
        
        public String Lottery { get; set; }
    }
}