﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPSENH_Test_Backend
{
    public class HelloVersion
    {
        public Guid Id { get; set; }

        public String Message { get; set; }

        public String Version { get; set; }
    }
}
