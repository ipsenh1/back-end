﻿using System;

namespace IPSENH_Test_Backend
{
    public class Owner
    {
        public Guid Id { get; set; }
        
        public String Name { get; set; }
        
        public String Email { get; set; }
        
        public String PhoneNumber { get; set; }
    }
}