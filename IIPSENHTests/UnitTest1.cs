using IPSENH_Test_Backend;
using IPSENH_Test_Backend.Controllers;
using IPSENH_Test_Backend.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace IIPSENHTests
{
    [TestClass]
    public  class UnitTest1
    {
        [TestMethod]
        public async Task TestMethod1()
        {
            //Arrange
            TestController controller = new TestController();

            //Act
            OkObjectResult response = await controller.GetTestString() as OkObjectResult;

            Assert.AreEqual(response.Value, "teststring");
        }

        [TestMethod]
        public async Task Test_HelloVersionsController_Get_ShouldReturnTest() {
            //Arrange
            var mockRepo = new Mock<IHelloVersionRepository>();
            mockRepo.CallBase = true;
            mockRepo.Setup(x => x.Get()).Returns(Task.FromResult(new HelloVersion { Version = "1.2.3", Message = "teststring" }));

            HelloVersionsController hvc = new HelloVersionsController(mockRepo.Object);

            //Act
            IActionResult result = await hvc.Get();
            HelloVersion hvResult = (result as OkObjectResult).Value as HelloVersion;

            //Arrange
            Assert.AreEqual("teststring", hvResult.Message);


        }
    }
}
